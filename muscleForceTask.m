%% config
side = 'LT';
muscleName = 'bicep';
numTrials = 5;
jitter = 0.5;
forceTime = 5;
restTime = 6;
validforceTimes = [forceTime-jitter forceTime forceTime+jitter];
validrestTimes = [restTime-jitter restTime restTime+jitter];
%%
syncState = '0';
taskState = '0';
% connect to sync trigger
serialSyncTrigger = connectSyncTrigger;
pause(5);
% Trigger Noraxon recording here
syncState = '1';
MsgToSend = ['<' syncState taskState];
write(serialSyncTrigger,MsgToSend,"char")

countdownbeep;

timingids = randi(length(validforceTimes),1,numTrials);
forceTimes = validforceTimes(timingids);
timingids = randi(length(validrestTimes),1,numTrials);
restTimes = validrestTimes(timingids);

for itrial = 1:numTrials
    currentForceTime = forceTimes(itrial);
    startbeep(0.5);
    taskState = '1';
    MsgToSend = ['<' syncState taskState];
    write(serialSyncTrigger,MsgToSend,"char")    
    lastTime = clock;
    while etime(clock, lastTime) < currentForceTime
        pause(0.01);
    end
    
    stopbeep(0.5);
    taskState = '0';
    MsgToSend = ['<' syncState taskState];
    write(serialSyncTrigger,MsgToSend,"char")
    currentRestTime = restTimes(itrial);
    lastTime = clock;
    while etime(clock, lastTime) < currentRestTime
        pause(0.01);
    end    
end

% Stop Noraxon recording here
syncState = '0';
MsgToSend = ['<' syncState taskState];
write(serialSyncTrigger,MsgToSend,"char")
%%
syncState = '0';
taskState = '0';
MsgToSend = ['<' syncState taskState];
write(serialSyncTrigger,MsgToSend,"char")
disconnectSyncTrigger(serialSyncTrigger);