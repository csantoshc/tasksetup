// Serial Monitor on
#define MON_ON

const int sync_pin = 2;
const int task_pin = 4;
int incomingByte;
byte sync_task_state[2];

void setup() {
  // put your setup code here, to run once:
  #ifdef MON_ON
  Serial.begin(9600);  // 9600 may be better for some micros to prevent interfering with wifi chip traffic
  #endif
  
  pinMode(sync_pin, OUTPUT);
  digitalWrite(sync_pin,LOW);
  pinMode(task_pin, OUTPUT);
  digitalWrite(task_pin,LOW);
  Serial.println("Sync trigger running...");
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();
    if (incomingByte == '<') {
      Serial.println("Started to scan for task state...");
      Serial.readBytes(sync_task_state,2);
      if (sync_task_state[0] == '1'){            
        digitalWrite(sync_pin,HIGH);
        Serial.println("Sync state went HIGH");
        //Serial.println(sync_task_state[0]);
        //Serial.println(sync_task_state[1]);
      }else if (sync_task_state[0] == '0'){
        digitalWrite(sync_pin,LOW);
        Serial.println("Sync state went LOW");
        //Serial.println(sync_task_state[0]);
        //Serial.println(sync_task_state[1]);
      }
      if (sync_task_state[1] == '1'){      
        digitalWrite(task_pin,HIGH);
        Serial.println("Task state went HIGH");
        //Serial.println(sync_task_state[0]);
        //Serial.println(sync_task_state[1]);
      }else if (sync_task_state[1] == '0'){
        digitalWrite(task_pin,LOW);
        Serial.println("Task state went LOW");
        //Serial.println(sync_task_state[0]);
        //Serial.println(sync_task_state[1]);
      }
    }
  }
}
