function serialSyncTrigger = connectSyncTrigger
    currmachine = getenv('COMPUTERNAME');
    switch currmachine
        case 'CBEM-NB-01'
            serialSyncTrigger = setupSerial('COM6');
        case 'CBEM-NB-03'
            serialSyncTrigger = setupSerial('COM3');
        case 'LW10MH350BEM002'
            serialSyncTrigger = setupSerial('COM5'); % Laptop = wired port - COM10; Bluetooth port COM9
        otherwise
            prompt = {'Enter COM port number for the Arduino handling sync pulse'};
            title = 'Sync Trigger COM port';
            dims = [1 75];
            definput = {'COM'};
            portno = cell2mat(inputdlg(prompt,title,dims,definput));
            serialSyncTrigger = setupSerial(portno);
    end
    if serialSyncTrigger == -1
        SyncTriggerConnected = false;        
    else
        SyncTriggerConnected = true;                
        fprintf('Connected to Sync Trigger\n');
    end