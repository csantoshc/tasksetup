function stopbeep(beeplength)

TimerH = timer('Period', 1, 'ExecutionMode', 'fixedRate', ...
               'TimerFcn', {@TimerCallback, beeplength}, 'TasksToExecute', 1);
start(TimerH);

function TimerCallback(TimerH, EventData, beeplength)
Fs = 14400; % Sampling Frequency.
% beeplength = 0.5;
t = linspace(0, beeplength, beeplength*(Fs)); % One Second Time Vector.
w = 2*pi*200; % Radian Value To Create 200 Hz Tone.
s = sin(w*t); % Create Tone.
sound(s, Fs)