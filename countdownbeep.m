function countdownbeep
Fs = 14400; % Sampling Frequency.
beeplen = 0.1;
t = linspace(0, beeplen, beeplen*(Fs)); % One Second Time Vector.
w = 2*pi*2000; % Radian Value To Create 1kHz Tone.
s = sin(w*t); % Create Tone.
ibeep = 1;

while ibeep <=3    
    sound(s, Fs)
    ibeep = ibeep + 1;
    lastTime = clock;
    while etime(clock, lastTime) < 1
        pause(0.01);
    end
end
