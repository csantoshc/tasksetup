function disconnectSyncTrigger(serialSyncTrigger)
% Close existing serial com port
    delete(serialSyncTrigger);
    fprintf('Disconnected Sync Trigger\n');